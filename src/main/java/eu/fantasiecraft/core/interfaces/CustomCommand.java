package eu.fantasiecraft.core.interfaces;

public abstract class CustomCommand {

    public abstract void setup();
    public boolean enabled;
}
