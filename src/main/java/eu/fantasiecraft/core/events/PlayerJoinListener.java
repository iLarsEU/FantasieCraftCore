package eu.fantasiecraft.core.events;

import eu.fantasiecraft.core.Base;
import eu.fantasiecraft.core.interfaces.CustomEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.*;

public class PlayerJoinListener extends CustomEvent implements Listener {

    @EventHandler
    public void on(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        setScoreboard(player);
    }

    @Override
    public void register() {
        Bukkit.getPluginManager().registerEvents(this, Base.getInstance());
    }
    public void setScoreboard(Player player) {
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective objective = scoreboard.registerNewObjective("FantasieCraft", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(ChatColor.GOLD + "FantasieCraft");

        Score online = objective.getScore(ChatColor.GRAY + "Online spelers");
        if(Bukkit.getOnlinePlayers().size() == 0) {
            online.setScore(0);
        } else {
            online.setScore(Bukkit.getOnlinePlayers().size());
        }
        Score money = objective.getScore(ChatColor.GRAY + "Gouden munten");
        money.setScore((int) Base.getEconomy().getBalance(player));
        player.setScoreboard(scoreboard);
    }

}
