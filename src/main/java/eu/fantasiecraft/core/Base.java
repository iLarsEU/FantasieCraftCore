package eu.fantasiecraft.core;

import com.earth2me.essentials.Essentials;
import eu.fantasiecraft.core.managers.CommandManager;
import eu.fantasiecraft.core.managers.EventManager;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

public class Base extends JavaPlugin {

    public static Base instance;

    private static Economy econ = null;
    private static Essentials ess = null;

    @Override
    public void onEnable() {
        instance = this;
        ess = (Essentials) Bukkit.getPluginManager().getPlugin("Essentials");

        CommandManager commandManager = new CommandManager();
        EventManager eventManager = new EventManager();

        if(!setupEconomy()) {
            Logger.getLogger("Minecraft").severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
    }
    @Override
    public void onDisable() {
        instance = null;
    }
    public static Base getInstance() {
        return instance;
    }
    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
    public static Economy getEconomy() {
        return econ;
    }
}
