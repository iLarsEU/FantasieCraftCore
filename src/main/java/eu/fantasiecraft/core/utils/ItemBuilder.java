package eu.fantasiecraft.core.utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.List;

public class ItemBuilder {

    public static ItemStack build(Material material, int amount, short code, String name, List<String> lore) {
        ItemStack itemStack = new ItemStack(material, amount, code);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(name);
        itemMeta.setLore(lore);
        itemStack.setItemMeta(itemMeta);

        return itemStack;
    }
    public static ItemStack buildHead(String name, String player) {
        ItemStack head = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta im = (SkullMeta) head.getItemMeta();
        im.setOwner(player);
        im.setDisplayName(ChatColor.GRAY + "Operator: " + player);
        head.setItemMeta(im);
        return head;
    }
}
