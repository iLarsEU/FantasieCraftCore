package eu.fantasiecraft.core.managers;

import eu.fantasiecraft.core.interfaces.CustomCommand;

import java.util.ArrayList;

public class CommandManager {

    ArrayList<CustomCommand> commands = new ArrayList<>();

    public CommandManager() {
        commands.add(new WapitiCommand());

        // --- end of command adding ---

        for(CustomCommand command : commands) {
            command.setup();
        }
    }
}
