package eu.fantasiecraft.core.managers;

import eu.fantasiecraft.core.events.PlayerJoinListener;
import eu.fantasiecraft.core.interfaces.CustomEvent;

import java.util.ArrayList;

public class EventManager {

    ArrayList<CustomEvent> events = new ArrayList<>();

    public EventManager() {
        events.add(new InventoryClickListener());
        events.add(new PlayerJoinListener());

        // --- end of event adding ---

        for (CustomEvent event : events) {
            event.register();
        }
    }
}
